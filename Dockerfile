FROM debian:jessie-slim
MAINTAINER Hoang Bui

ENV DEBIAN_FRONTEND=noninteractive \
    PHP_UPLOAD_MAX_FILESIZE=10M \
    PHP_POST_MAX_SIZE=10M \
    APACHE_CONFDIR=/etc/apache2 \
    APACHE_ENVVARS=$APACHE_CONFDIR/envvars \
    APACHE_RUN_USER=www-data \
    APACHE_RUN_GROUP=www-data \
    APACHE_RUN_DIR=/var/run/apache2 \
    APACHE_PID_FILE=$APACHE_RUN_DIR/apache2.pid \
    APACHE_LOCK_DIR=/var/lock/apache2 \
    APACHE_LOG_DIR=/var/log/apache2 \
    LANG=C

ADD src /src

RUN apt-get update && \
    apt-get -y install supervisor git apache2 libapache2-mod-php5 mysql-server php5-mysql pwgen php-apc php5-mcrypt && \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf && \
    rm -rf /var/lib/mysql/* && \
    a2enmod rewrite && \
    apt-get install -y phpmyadmin && \
    ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf && \
    a2enconf phpmyadmin && \
    mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR && \
    cp /src/start-apache2.sh /start-apache2.sh && \
    cp /src/start-mysqld.sh /start-mysqld.sh && \
    cp /src/run.sh /run.sh && \
    cp /src/my.cnf /etc/mysql/conf.d/my.cnf && \
    cp /src/supervisord-apache2.conf /etc/supervisor/conf.d/supervisord-apache2.conf && \
    cp /src/supervisord-mysqld.conf /etc/supervisor/conf.d/supervisord-mysqld.conf && \
    cp /src/apache_default /etc/apache2/sites-available/000-default.conf && \
    cp /src/create_mysql_admin_user.sh /create_mysql_admin_user.sh && \
    rm -rf /src && \
    chmod 755 /*.sh

VOLUME  ["/etc/mysql", "/var/lib/mysql" ]

EXPOSE 80 3306
CMD ["/run.sh"]
